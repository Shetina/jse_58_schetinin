package ru.t1.schetinin.tm.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.schetinin.tm.dto.model.AbstractModelDTO;

import javax.persistence.EntityManager;
import java.util.Comparator;
import java.util.List;

public interface IDTORepository<M extends AbstractModelDTO> {

    @NotNull
    M add(@NotNull M model) throws Exception;

    void clear() throws Exception;

    boolean existsById(@NotNull String id) throws Exception;

    @Nullable
    List<M> findAll() throws Exception;

    @Nullable
    List<M> findAll(@Nullable Comparator comparator) throws Exception;

    @Nullable
    M findOneById(@NotNull String id) throws Exception;

    int getSize() throws Exception;

    void remove(@NotNull M model) throws Exception;

    void update(@NotNull M model) throws Exception;

    @NotNull
    EntityManager getEntityManager();

}
