package ru.t1.schetinin.tm.service.dto;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;
import ru.t1.schetinin.tm.api.repository.dto.ITaskDTORepository;
import ru.t1.schetinin.tm.api.service.dto.ITaskDTOService;
import ru.t1.schetinin.tm.dto.model.TaskDTO;
import ru.t1.schetinin.tm.enumerated.Status;
import ru.t1.schetinin.tm.exception.entity.TaskNotFoundException;
import ru.t1.schetinin.tm.exception.field.DescriptionEmptyException;
import ru.t1.schetinin.tm.exception.field.IdEmptyException;
import ru.t1.schetinin.tm.exception.field.NameEmptyException;
import ru.t1.schetinin.tm.exception.field.StatusEmptyException;
import ru.t1.schetinin.tm.exception.user.UserIdEmptyException;

import javax.persistence.EntityManager;
import java.util.Collections;
import java.util.List;

@Service
@NoArgsConstructor
public final class TaskDTOService extends AbstractUserOwnedDTOService<TaskDTO, ITaskDTORepository> implements ITaskDTOService {

    @NotNull
    protected ITaskDTORepository getRepository() {
        return context.getBean(ITaskDTORepository.class);
    }

    @Override
    public void changeTaskStatusById(@Nullable final String userId, @Nullable final String id, @Nullable final Status status) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (status == null) throw new StatusEmptyException();
        @Nullable final TaskDTO task = findOneById(userId, id);
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(status);
        @NotNull final ITaskDTORepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            repository.update(userId, task);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    public TaskDTO create(@Nullable final String userId, @Nullable final String name) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        @NotNull TaskDTO task = new TaskDTO();
        task.setUserId(userId);
        task.setName(name);
        @NotNull final ITaskDTORepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            repository.add(userId, task);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return task;
    }

    @NotNull
    @Override
    public TaskDTO create(@Nullable final String userId, @Nullable final String name, @Nullable final String description) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        @NotNull TaskDTO task = new TaskDTO();
        task.setUserId(userId);
        task.setName(name);
        task.setDescription(description);
        @NotNull final ITaskDTORepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            repository.add(userId, task);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return task;
    }

    @Nullable
    @Override
    public List<TaskDTO> findAllByProjectId(@Nullable final String userId, @Nullable String projectId) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) return Collections.emptyList();
        @NotNull final ITaskDTORepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            return repository.findAllByProjectId(userId, projectId);
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void updateById(@Nullable final String userId, @Nullable final String id, @Nullable final String name, @Nullable final String description) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        @Nullable final TaskDTO task = findOneById(userId, id);
        if (task == null) throw new TaskNotFoundException();
        task.setName(name);
        task.setDescription(description);
        @NotNull final ITaskDTORepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            repository.update(userId, task);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void updateProjectIdById(@Nullable final String userId, @Nullable final String id, @Nullable final String projectId) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable final TaskDTO task = findOneById(userId, id);
        if (task == null) throw new TaskNotFoundException();
        task.setProjectId(projectId);
        @NotNull final ITaskDTORepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            repository.update(userId, task);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

}