package ru.t1.schetinin.tm.repository.model;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.t1.schetinin.tm.api.repository.model.ITaskRepository;
import ru.t1.schetinin.tm.model.Task;

import javax.persistence.EntityManager;
import java.util.List;

@Repository
@Scope("prototype")
@NoArgsConstructor
public class TaskRepository extends AbstractUserOwnedRepository<Task> implements ITaskRepository {

    @NotNull
    @Override
    public Class<Task> getEntity() {
        return Task.class;
    }

    @NotNull
    @Override
    public Task create(@NotNull final String userId, @NotNull final String name, @NotNull final String description) throws Exception {
        @NotNull final Task task = new Task(name, description);
        return add(userId, task);
    }

    @NotNull
    @Override
    public Task create(@NotNull final String userId, @NotNull final String name) throws Exception {
        @NotNull final Task task = new Task(name);
        return add(userId, task);
    }

    @Nullable
    @Override
    public List<Task> findAllByProjectId(@NotNull final String userId, @NotNull final String projectId) throws Exception {
        @NotNull final String jpql = "SELECT m FROM Task m WHERE m.user.id = :userId AND m.project.id = :projectId";
        return entityManager.createQuery(jpql, getEntity())
                .setParameter("userId", userId)
                .setParameter("projectId", projectId)
                .getResultList();
    }

}