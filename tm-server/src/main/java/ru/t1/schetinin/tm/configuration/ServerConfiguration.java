package ru.t1.schetinin.tm.configuration;

import lombok.SneakyThrows;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Environment;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import ru.t1.schetinin.tm.api.service.IPropertyService;
import ru.t1.schetinin.tm.dto.model.ProjectDTO;
import ru.t1.schetinin.tm.dto.model.SessionDTO;
import ru.t1.schetinin.tm.dto.model.TaskDTO;
import ru.t1.schetinin.tm.dto.model.UserDTO;
import ru.t1.schetinin.tm.model.Project;
import ru.t1.schetinin.tm.model.Session;
import ru.t1.schetinin.tm.model.Task;
import ru.t1.schetinin.tm.model.User;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import java.util.HashMap;
import java.util.Map;

@Configuration
@ComponentScan("ru.t1.schetinin.tm")
public class ServerConfiguration {

    @Bean
    @NotNull
    @SneakyThrows
    public EntityManagerFactory entityManagerFactory(@NotNull IPropertyService propertyService) {
        @NotNull final Map<String, String> settings = new HashMap<>();
        settings.put(Environment.DRIVER, propertyService.getDBDriver());
        settings.put(Environment.URL, propertyService.getDBUrl());
        settings.put(Environment.USER, propertyService.getDBUser());
        settings.put(Environment.PASS, propertyService.getDBPassword());
        settings.put(Environment.DIALECT, propertyService.getDBDialect());
        settings.put(Environment.HBM2DDL_AUTO, propertyService.getDBHbm2ddlAuto());
        settings.put(Environment.SHOW_SQL, propertyService.getDBShowSql());
        settings.put(Environment.FORMAT_SQL, propertyService.getDBFormatSql());
        settings.put(Environment.USE_SECOND_LEVEL_CACHE, propertyService.getDBSecondLvlCache());
        settings.put(Environment.CACHE_REGION_FACTORY, propertyService.getDBFactoryClass());
        settings.put(Environment.USE_QUERY_CACHE, propertyService.getDBUseQueryCache());
        settings.put(Environment.USE_MINIMAL_PUTS, propertyService.getDBUseMinPuts());
        settings.put(Environment.CACHE_REGION_PREFIX, propertyService.getDBRegionPrefix());
        settings.put(Environment.CACHE_PROVIDER_CONFIG, propertyService.getDBConfigFilePath());
        @NotNull final StandardServiceRegistryBuilder registryBuilder = new StandardServiceRegistryBuilder();
        registryBuilder.applySettings(settings);
        @NotNull final StandardServiceRegistry registry = registryBuilder.build();
        @NotNull final MetadataSources source = new MetadataSources(registry);
        source.addAnnotatedClass(ProjectDTO.class);
        source.addAnnotatedClass(Project.class);
        source.addAnnotatedClass(TaskDTO.class);
        source.addAnnotatedClass(Task.class);
        source.addAnnotatedClass(UserDTO.class);
        source.addAnnotatedClass(User.class);
        source.addAnnotatedClass(SessionDTO.class);
        source.addAnnotatedClass(Session.class);
        @NotNull final Metadata metadata = source.getMetadataBuilder().build();
        return metadata.getSessionFactoryBuilder().build();
    }

    @Bean
    @NotNull
    @SneakyThrows
    @Scope("prototype")
    public EntityManager entityManager(@NotNull final EntityManagerFactory entityManagerFactory) {
        return entityManagerFactory.createEntityManager();
    }

}