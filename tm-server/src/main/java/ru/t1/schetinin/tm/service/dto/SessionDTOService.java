package ru.t1.schetinin.tm.service.dto;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Service;
import ru.t1.schetinin.tm.api.repository.dto.ISessionDTORepository;
import ru.t1.schetinin.tm.api.service.dto.ISessionDTOService;
import ru.t1.schetinin.tm.dto.model.SessionDTO;

@Service
@NoArgsConstructor
public final class SessionDTOService extends AbstractUserOwnedDTOService<SessionDTO, ISessionDTORepository> implements ISessionDTOService {
    @NotNull
    protected ISessionDTORepository getRepository() {
        return context.getBean(ISessionDTORepository.class);
    }

}