package ru.t1.schetinin.tm.exception.system;

import org.jetbrains.annotations.NotNull;

public class ArgumentNotSupportedException extends AbstractSystemException {

    public ArgumentNotSupportedException() {
        super("Error! Argument not supported...");
    }

    public ArgumentNotSupportedException(@NotNull final String argument) {
        super("Error! Argument ''" + argument + "'' not supported...");
    }

}
