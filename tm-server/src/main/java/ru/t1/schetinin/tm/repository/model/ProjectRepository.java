package ru.t1.schetinin.tm.repository.model;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.t1.schetinin.tm.api.repository.model.IProjectRepository;
import ru.t1.schetinin.tm.dto.model.ProjectDTO;
import ru.t1.schetinin.tm.model.Project;

import javax.persistence.EntityManager;

@Repository
@Scope("prototype")
@NoArgsConstructor
public class ProjectRepository extends AbstractUserOwnedRepository<Project> implements IProjectRepository {

    @NotNull
    @Override
    public Class<Project> getEntity() {
        return Project.class;
    }

    @NotNull
    @Override
    public Project create(@NotNull final String userId, @NotNull final String name, @NotNull final String description) throws Exception {
        @NotNull final Project project = new Project(name, description);
        return add(userId, project);
    }

    @NotNull
    @Override
    public Project create(@NotNull final String userId, @NotNull final String name) throws Exception {
        @NotNull final Project project = new Project(name);
        return add(userId, project);
    }

}