package ru.t1.schetinin.tm.repository.model;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.t1.schetinin.tm.api.repository.model.IUserOwnedRepository;
import ru.t1.schetinin.tm.model.AbstractUserOwnedModel;
import ru.t1.schetinin.tm.model.User;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.Comparator;
import java.util.List;

@Repository
@Scope("prototype")
@NoArgsConstructor
public abstract class AbstractUserOwnedRepository<M extends AbstractUserOwnedModel> extends AbstractRepository<M> implements IUserOwnedRepository<M> {

    @NotNull
    @Override
    public M add(@NotNull final String userId, @NotNull final M model) throws Exception {
        model.setUser(entityManager.find(User.class, userId));
        entityManager.persist(model);
        return model;
    }

    @Override
    public void clear(@NotNull final String userId) throws Exception {
        @Nullable final List<M> models = findAll(userId);
        if (models == null) return;
        for (@NotNull final M model : models) {
            remove(model);
        }
    }

    @Override
    public boolean existsById(@NotNull final String userId, @NotNull final String id) throws Exception {
        return findOneById(userId, id) != null;
    }

    @Nullable
    @Override
    public List<M> findAll(@NotNull final String userId) throws Exception {
        @NotNull final String jpql = "SELECT m FROM " + getEntity().getSimpleName() + " m WHERE m.user.id = :userId";
        return entityManager.createQuery(jpql, getEntity())
                .setParameter("userId", userId)
                .getResultList();
    }

    @Nullable
    @Override
    public List<M> findAll(@NotNull final String userId, @Nullable final Comparator comparator) throws Exception {
        @NotNull final CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        @NotNull final CriteriaQuery<M> criteriaQuery = criteriaBuilder.createQuery(getEntity());
        @NotNull final Root<M> root = criteriaQuery.from(getEntity());
        criteriaQuery.select(root);
        criteriaQuery.where(criteriaBuilder.equal(root.get("userId"), userId));
        criteriaQuery.orderBy(criteriaBuilder.asc(root.get(getSortType(comparator))));
        return entityManager.createQuery(criteriaQuery).getResultList();
    }

    @Nullable
    @Override
    public M findOneById(@NotNull final String userId, @NotNull final String id) throws Exception {
        @NotNull final String jpql = "SELECT m FROM " + getEntity().getSimpleName() + " m WHERE m.user.id = :userId AND m.id = :id";
        return entityManager.createQuery(jpql, getEntity())
                .setParameter("userId", userId)
                .setParameter("id", id)
                .setMaxResults(1)
                .getResultList().stream().findFirst().orElse(null);
    }

    @Override
    public int getSize(@NotNull final String userId) throws Exception {
        @NotNull final String jpql = "SELECT COUNT(m) FROM " + getEntity().getSimpleName() + " m WHERE m.user.id = :userId";
        return entityManager.createQuery(jpql, Long.class)
                .setParameter("userId", userId)
                .getSingleResult().intValue();
    }

    @Override
    public void remove(@NotNull final String userId, @NotNull final M model) throws Exception {
        entityManager.remove(model);
    }

    @Override
    public void update(@NotNull final String userId, @NotNull final M model) throws Exception {
        model.setUser(entityManager.find(User.class, userId));
        entityManager.merge(model);
    }

}