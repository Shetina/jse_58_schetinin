package ru.t1.schetinin.tm.dto.response;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.t1.schetinin.tm.dto.model.TaskDTO;

@Getter
@Setter
@NoArgsConstructor
public class TaskChangeStatusByIndexResponse extends AbstractTaskResponse {

    public TaskChangeStatusByIndexResponse(@NotNull final TaskDTO task) {
        super(task);
    }

}