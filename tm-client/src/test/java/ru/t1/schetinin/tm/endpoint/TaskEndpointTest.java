package ru.t1.schetinin.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.t1.schetinin.tm.api.endpoint.IAuthEndpoint;
import ru.t1.schetinin.tm.api.endpoint.IProjectEndpoint;
import ru.t1.schetinin.tm.api.endpoint.ITaskEndpoint;
import ru.t1.schetinin.tm.api.endpoint.IUserEndpoint;
import ru.t1.schetinin.tm.api.service.IPropertyService;
import ru.t1.schetinin.tm.dto.request.*;
import ru.t1.schetinin.tm.enumerated.Status;
import ru.t1.schetinin.tm.marker.SoapCategory;
import ru.t1.schetinin.tm.dto.model.TaskDTO;
import ru.t1.schetinin.tm.service.PropertyService;

import java.util.List;

@Category(SoapCategory.class)
public class TaskEndpointTest {

    @NotNull
    private static final IPropertyService PROPERTY_SERVICE = new PropertyService();

    @NotNull
    private static final IAuthEndpoint AUTH_ENDPOINT = IAuthEndpoint.newInstance(PROPERTY_SERVICE);

    @NotNull
    private static final IUserEndpoint USER_ENDPOINT = IUserEndpoint.newInstance(PROPERTY_SERVICE);

    @NotNull
    private static final IProjectEndpoint PROJECT_ENDPOINT = IProjectEndpoint.newInstance(PROPERTY_SERVICE);

    @NotNull
    private static final ITaskEndpoint TASK_ENDPOINT = ITaskEndpoint.newInstance(PROPERTY_SERVICE);

    @Nullable
    private static String adminToken;

    @Nullable
    private static String userToken;

    @Nullable
    private String projectId1;

    @Nullable
    private String taskId1;

    private int taskIndex1;

    @Nullable
    private String taskId2;

    private int taskIndex2;

    @BeforeClass
    public static void setUp() {
        @NotNull final UserLoginRequest loginRequest = new UserLoginRequest();
        loginRequest.setLogin("admin");
        loginRequest.setPassword("admin");
        adminToken = AUTH_ENDPOINT.login(loginRequest).getToken();
        @NotNull final UserRegistryRequest request = new UserRegistryRequest(adminToken);
        request.setLogin("userTestLogin");
        request.setPassword("userTestPassword");
        request.setEmail("testEmail");
        USER_ENDPOINT.registryUser(request);
        @NotNull final UserLoginRequest loginUserRequest = new UserLoginRequest();
        loginUserRequest.setLogin("userTestLogin");
        loginUserRequest.setPassword("userTestPassword");
        userToken = AUTH_ENDPOINT.login(loginUserRequest).getToken();
    }

    @AfterClass
    public static void tearDown() {
        @NotNull final UserRemoveRequest request = new UserRemoveRequest(adminToken);
        request.setLogin("userTestLogin");
        USER_ENDPOINT.removeUser(request);
    }

    @Before
    public void before() {
        taskId1 = createTestTask("userTask1Name", "userTask1Description");
        taskIndex1 = 0;
        taskId2 = createTestTask("userTask2Name", "userTask2Description");
        taskIndex2 = 1;
        projectId1 = createTestProject("userProject1Name", "userProject1Description");
    }

    @After
    public void after() {
        @NotNull final TaskClearRequest request = new TaskClearRequest(userToken);
        Assert.assertNotNull(TASK_ENDPOINT.clearTask(request));
    }

    @NotNull
    private String createTestTask(final String name, final String description) {
        @NotNull final TaskCreateRequest taskCreateRequest = new TaskCreateRequest(userToken);
        taskCreateRequest.setName(name);
        taskCreateRequest.setDescription(description);
        return TASK_ENDPOINT.createTask(taskCreateRequest).getTask().getId();
    }

    @NotNull
    private String createTestProject(final String name, final String description) {
        @NotNull final ProjectCreateRequest projectCreateRequest = new ProjectCreateRequest(userToken);
        projectCreateRequest.setName(name);
        projectCreateRequest.setDescription(description);
        return PROJECT_ENDPOINT.createProject(projectCreateRequest).getProject().getId();
    }

    @Nullable
    private TaskDTO findTestTaskById(final String id) {
        @Nullable final TaskShowByIdRequest request = new TaskShowByIdRequest(userToken);
        request.setId(id);
        return TASK_ENDPOINT.showTaskById(request).getTask();
    }

    @Test
    public void changeStatusByIdTask() {
        @NotNull final Status status = Status.COMPLETED;
        @NotNull final TaskChangeStatusByIdRequest taskCreateRequestNullToken = new TaskChangeStatusByIdRequest(null);
        taskCreateRequestNullToken.setId(taskId1);
        taskCreateRequestNullToken.setStatus(status);
        Assert.assertThrows(Exception.class, () -> TASK_ENDPOINT.changeTaskStatusById(taskCreateRequestNullToken));
        @NotNull final TaskChangeStatusByIdRequest request = new TaskChangeStatusByIdRequest(userToken);
        request.setId(taskId1);
        request.setStatus(status);
        Assert.assertNotNull(TASK_ENDPOINT.changeTaskStatusById(request));
        @Nullable final TaskDTO task = findTestTaskById(taskId1);
        Assert.assertNotNull(task);
        Assert.assertEquals(status, task.getStatus());
    }

    @Test
    public void clearTask() {
        @NotNull final TaskClearRequest request = new TaskClearRequest(userToken);
        TASK_ENDPOINT.clearTask(request);
        @NotNull final TaskListRequest taskListRequest = new TaskListRequest(userToken);
        Assert.assertNull(TASK_ENDPOINT.listTask(taskListRequest).getTasks());
    }

    @Test
    public void createTask() {
        @NotNull final TaskCreateRequest taskCreateRequest = new TaskCreateRequest(userToken);
        taskCreateRequest.setName("userTaskName3");
        taskCreateRequest.setDescription("userTask3Description");
        @Nullable TaskDTO task = TASK_ENDPOINT.createTask(taskCreateRequest).getTask();
        Assert.assertNotNull(task);
        Assert.assertEquals("userTaskName3", task.getName());
        Assert.assertEquals("userTask3Description", task.getDescription());
    }

    @Test
    public void completeByIdTask() {
        @NotNull final TaskCompleteByIdRequest request = new TaskCompleteByIdRequest(userToken);
        request.setId(taskId1);
        Assert.assertNotNull(TASK_ENDPOINT.completeTaskById(request));
        @Nullable TaskDTO task = findTestTaskById(taskId1);
        Assert.assertNotNull(task);
        Assert.assertEquals(Status.COMPLETED, task.getStatus());
    }

    @Test
    public void listTask() {
        @NotNull final TaskListRequest request = new TaskListRequest(userToken);
        @Nullable final List<TaskDTO> tasks = TASK_ENDPOINT.listTask(request).getTasks();
        Assert.assertNotNull(tasks);
        Assert.assertEquals(2, tasks.size());
        for (TaskDTO task : tasks) {
            Assert.assertNotNull(findTestTaskById(task.getId()));
        }
    }

    @Test
    public void removeByIdTask() {
        @NotNull final TaskRemoveByIdRequest request = new TaskRemoveByIdRequest(userToken);
        request.setId(taskId2);
        TASK_ENDPOINT.removeTaskById(request);
        Assert.assertNull(findTestTaskById(taskId2));
    }

    @Test
    public void showByIdTask() {
        @NotNull final TaskShowByIdRequest request = new TaskShowByIdRequest(userToken);
        request.setId(taskId1);
        @Nullable final TaskDTO task = TASK_ENDPOINT.showTaskById(request).getTask();
        Assert.assertNotNull(task);
        Assert.assertEquals(taskId1, task.getId());
    }

    @Test
    public void startByIdTask() {
        @NotNull final TaskStartByIdRequest request = new TaskStartByIdRequest(userToken);
        request.setId(taskId1);
        Assert.assertNotNull(TASK_ENDPOINT.startTaskById(request));
        @Nullable TaskDTO task = findTestTaskById(taskId1);
        Assert.assertNotNull(task);
        Assert.assertEquals(Status.IN_PROGRESS, task.getStatus());
    }

    @Test
    public void updateByIdTask() {
        @NotNull final TaskUpdateByIdRequest request = new TaskUpdateByIdRequest(userToken);
        request.setId(taskId1);
        request.setName("userTaskName3");
        request.setDescription("userTask3Description");
        Assert.assertNotNull(TASK_ENDPOINT.updateTaskById(request));
        @Nullable TaskDTO task = findTestTaskById(taskId1);
        Assert.assertNotNull(task);
        Assert.assertEquals("userTaskName3", task.getName());
        Assert.assertEquals("userTask3Description", task.getDescription());
    }

    private void bindTaskProject(final String projectId, final String taskId) {
        @NotNull final TaskBindToProjectRequest request = new TaskBindToProjectRequest(userToken);
        request.setProjectId(projectId);
        request.setTaskId(taskId);
        TASK_ENDPOINT.bindTaskToProject(request);
    }

    @Test
    public void bindToProjectTask() {
        @NotNull final TaskBindToProjectRequest request = new TaskBindToProjectRequest(userToken);
        request.setProjectId(projectId1);
        request.setTaskId(taskId1);
        Assert.assertNotNull(TASK_ENDPOINT.bindTaskToProject(request));
        @NotNull final TaskShowByProjectIdRequest requestShow = new TaskShowByProjectIdRequest(userToken);
        requestShow.setProjectId(projectId1);
        @Nullable final List<TaskDTO> tasks = TASK_ENDPOINT.showTaskByProjectId(requestShow).getTasks();
        Assert.assertNotNull(tasks);
        Assert.assertEquals(1, tasks.size());
        Assert.assertEquals(projectId1, tasks.get(0).getProjectId());
        Assert.assertEquals(taskId1, tasks.get(0).getId());
    }

    @Test
    public void showByProjectIdTask() {
        bindTaskProject(projectId1, taskId1);
        bindTaskProject(projectId1, taskId2);
        @NotNull final TaskShowByProjectIdRequest request = new TaskShowByProjectIdRequest(userToken);
        request.setProjectId(projectId1);
        @Nullable final List<TaskDTO> tasks = TASK_ENDPOINT.showTaskByProjectId(request).getTasks();
        Assert.assertNotNull(tasks);
        Assert.assertEquals(2, tasks.size());
        for (TaskDTO task : tasks) {
            Assert.assertEquals(projectId1, task.getProjectId());
        }
    }

    @Test
    public void unbindFromProjectTask() {
        bindTaskProject(projectId1, taskId1);
        bindTaskProject(projectId1, taskId2);
        @NotNull final TaskUnbindFromProjectRequest request = new TaskUnbindFromProjectRequest(userToken);
        request.setProjectId(projectId1);
        request.setTaskId(taskId1);
        Assert.assertNotNull(TASK_ENDPOINT.unbindTaskFromProject(request));
        @NotNull final TaskShowByProjectIdRequest requestShow = new TaskShowByProjectIdRequest(userToken);
        requestShow.setProjectId(projectId1);
        @Nullable final List<TaskDTO> tasks = TASK_ENDPOINT.showTaskByProjectId(requestShow).getTasks();
        Assert.assertNotNull(tasks);
        Assert.assertEquals(1, tasks.size());
        Assert.assertEquals(projectId1, tasks.get(0).getProjectId());
        Assert.assertEquals(taskId2, tasks.get(0).getId());
    }

}
