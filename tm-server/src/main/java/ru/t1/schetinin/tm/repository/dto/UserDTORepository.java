package ru.t1.schetinin.tm.repository.dto;

import lombok.NoArgsConstructor;
import org.hibernate.annotations.QueryHints;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.t1.schetinin.tm.api.repository.dto.IUserDTORepository;
import ru.t1.schetinin.tm.dto.model.UserDTO;
import ru.t1.schetinin.tm.enumerated.Role;

import javax.persistence.EntityManager;

@Repository
@Scope("prototype")
@NoArgsConstructor
public class UserDTORepository extends AbstractDTORepository<UserDTO> implements IUserDTORepository {

    @NotNull
    @Override
    public Class<UserDTO> getEntity() {
        return UserDTO.class;
    }

    @NotNull
    @Override
    public UserDTO create(@NotNull final String login, @NotNull final String password) throws Exception {
        @NotNull final UserDTO user = new UserDTO();
        user.setLogin(login);
        user.setPasswordHash(password);
        user.setRole(Role.USUAL);
        return add(user);
    }

    @NotNull
    @Override
    public UserDTO create(@NotNull final String login, @NotNull final String password, @Nullable final String email) throws Exception {
        @NotNull final UserDTO user = create(login, password);
        user.setEmail((email == null) ? "" : email);
        return user;
    }

    @NotNull
    @Override
    public UserDTO create(@NotNull final String login, @NotNull final String password, @Nullable final Role role) throws Exception {
        @NotNull final UserDTO user = create(login, password);
        if (role != null) user.setRole(role);
        return user;
    }

    @Nullable
    @Override
    public UserDTO findByLogin(@NotNull final String login) throws Exception {
        @NotNull final String jpql = "SELECT m FROM UserDTO m WHERE m.login = :login";
        return entityManager.createQuery(jpql, getEntity())
                .setParameter("login", login)
                .setMaxResults(1)
                .setHint(QueryHints.CACHEABLE, true)
                .getResultList().stream().findFirst().orElse(null);
    }

    @Nullable
    @Override
    public UserDTO findByEmail(@NotNull final String email) throws Exception {
        @NotNull final String jpql = "SELECT m FROM UserDTO m WHERE m.email = :email";
        return entityManager.createQuery(jpql, getEntity())
                .setParameter("email", email)
                .setMaxResults(1)
                .setHint(QueryHints.CACHEABLE, true)
                .getResultList().stream().findFirst().orElse(null);
    }

    @Override
    public Boolean isLoginExists(@NotNull final String login) throws Exception {
        return findByLogin(login) != null;
    }

    @Override
    public Boolean isEmailExists(@NotNull final String email) throws Exception {
        return findByEmail(email) != null;
    }

}
